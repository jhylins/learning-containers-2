# Serving a Static Web Page from a Container in the Cloud.

The aim of this learning project is to understand how to run a docker container in the Google Cloud, which publishes a simple web page via the internet.

There is a detailed series of steps below if you want to try it yourself.

## Approach

The actual end result web site is deliberately very simple and underwhelming. This is so that I can focus on learning about docker and Google Cloud.

The first step is to create a Docker image, which includes the html file I want to serve, and a webserver to run in the container and respond to any requests for the html. After a bit of research I've used a lightweight web server (NGINX) to keep the docker image as simple and small as possible. Including NGINX in your Docker image is really simple. It requires just one line of code in the Dockerfile. Docker does the rest. If you look at the Dockerfile it descibes how the container image is built up.

It seems there are a number a ways to deploy containers in Google Cloud. I've chosen to use a Virtual Machine Instance (Google Compute Engine), running the Docker Engine.

Many of the steps here can be carried out via the Google Cloud GUI Browser Console, but I'll use the command line (Google Cloud SDK CLI) wherever possible, as in future training projects I'd like to automate the steps in a CI/CD pipeline. It's possible in the Google Cloud GUI Browser Console to show the equivalent CLI command via "Equivalent REST or command line" link on many pages. This is really helpful when constructing commands.

## Using Google Cloud

A personal Google Cloud account will be required, with billing set up. As I write, Google gives all new accounts free credit, so nothing here should cost any money. As a precaution, any Google Cloud projects set up should be deleted, to avoid any unexpected charges. There are a number of optional DECOMISSIONING steps listed below in Part 3.

## How to Get Started

1. Install Docker Desktop Community Edition on your machine. [Link to Downloads](https://hub.docker.com/search?type=edition&offering=community)  
2. Create a Google Account, and sign into [Google Cloud Platform](https://console.cloud.google.com/)  
3. Install [Google Cloud SDK](https://cloud.google.com/sdk/install) on your machine and [authorise Google Cloud SDK](https://cloud.google.com/sdk/docs/authorizing) to use your Google Cloud account.
4. Authorise gcloud as a Docker credential helper:  

```console
$ gcloud auth configure-docker
```

5. Copy this project from GitLab to your machine. If you're using git you can clone, using the `git clone git@gitlab.com:jhylins/learning-containers-2.git` command. Alternatively you can download from GitLab as a zip file.

## Part 1 - Building and Testing a Custom Docker Image Locally

This allows us to build, check and run our Docker container locally (on our machine), before attempting to run it in the cloud in Part 2.  

### How to Build the Docker Image

This step creates a custom docker image based on an open source webserver (NGINX), plus the html page in this project. The `Dockerfile` in this project is the recipe which describes how the layers of the cake (your Docker image) are built up. 

```console
$ docker build -t html-server-image:v1 .
```

The `build` command instructs docker to create an image from the `Dockerfile` instructions.  
The `-t` option tells docker to give the new image a name and a tag.
We specify that the image should be named `html-server-image` and tagged with the version number `v1`.  

### How to Check if the Image Exists Locally

```console
$ docker image ls
```

You should see your new docker image listed!  

### How to Run the Docker Container Locally

```console
$ docker run -d -p 80:80 html-server-image:v1
```

`-d` : Run in 'detached' mode, as we don't want to interact with the container from the command line.  
`-p 80:80` : Map network port 80 on the container to port 80 on the docker host. This is just so that the container can communicate with the outside world.  

### How to Check if the Docker Container is Running Locally

```console
$ docker container ls
```

You sould now see your new container listed!

(FYI We could also use docker ps command here)  

## Part 2 - Manually Pushing to the Cloud and Running

Now that we've established that we can build and run a container on our own machine, it's time to send our image to the Google Cloud, so that it's available to create containers there. I've chosen to do this using `gcloud` commands in my command line locally, but equally you could achieve the same result via the Google Cloud Console, which is a much friendlier interface. When you log into your [Google Cloud Platform](https://console.cloud.google.com/) account (see above) you get a link to the console.  

### Create a New Project in Google Cloud

Everything you do in the Google Cloud has to be related to a Google Cloud Project, so lets create one by running the following command on your own machine. It will communicate with Google Cloud and set up the project.

```console
$ glcoud projects create [PROJECT_ID]
```

FYI You may already have done this when you created your Google Cloud Account (see How to Get Started). You can check using the command below if you're not sure.  

### Confirm the New Project Exists

```console
$ glcoud projects list
```

### Ensure the New Project has Billing Enabled

You will need to ensure this project up for billing, separately, via the Google Cloud Console (GUI). The console is just a pretty front end for Google Cloud, which you see when you log in via a browser.  

Billing > My projects > Actions > Change Billing > Set Account

### Enable the Container Registry API

At the moment our image is just on our own machine. So that we can `push` our local docker image up to the `Google Cloud Container Registry`, we need to run some gcloud commands. All this is on your local machine, via a command prompt. 

```console
$ gcloud config set project [PROJECT_ID]
$ glcoud services list
$ gcloud services enable containerregistry.googleapis.com
$ gcloud services list
```

These commands set the current project context to your new GCP project, then lists all the apis currently enabled, then enables the container registry api, then lists again to check it's appeared. Quite a lot here but really just making sure the project you created above has all the correct permissions to work with containers.  

### Tag the Image Locally

So that when we push the image it is pushed to the correct Google Cloud Container Registry, we have to give it a specific name, which includes our GCP project name. This is so that Google knows where to put the container image.   

```
$ docker tag html-server-image:v1 gcr.io/[PROJECT_ID]/html-server-image
```

### Push the Image to the Google Cloud Image Repository

```console
$ docker push gcr.io/[PROJECT ID]/html-server-image
```

### Confirm that the Container Image is in the Google Cloud Container Registry

```
$ gcloud container images list
```

### Start a Compute Instance VM Using the Image

```console
$ gcloud compute instances create-with-container instance-1 --container-image=[IMAGE_ID] --zone=europe-west2-c  --tags=http-server --machine-type=f1-micro
```

This is a long command, with lots of options set but in one step it creates and configures a VM and cretes a instance of our docker container. Remember to replace IMAGE_ID with the name of your image in the Google Container Registry.  You may decide it's easier to do this step in the browser based Google Cloud console, which has a wizard to take you though it.  

### Confirm the Instance is Running

```console
$ gcloud compute instances list
```

This command will also show the external IP address, which can be used to view the static web page in a browser!

## Part 3 - DECOMISSIONING

### DECOMISSIONING - Delete the Google Cloud VM Instance

```console
$ gcloud compute instances list
$ gcloud compute instances delete [INSTANCE_NAME]
$ gcloud compute instances list
```

### DECOMISSIONING - Delete the Google Cloud Project

```console
$ gcloud projects list
$ glcoud projects delete [PROJECT_ID]
$ gcloud projects list
```

### DECOMISSIONING - How to Stop and Remove a Container Locally

```console
$ docker ps
$ docker stop [CONTAINER ID]
$ docker rm [CONTAINER ID]
```

### DECOMISSIONING - How to Delete Docker Images Locally

```console
$ docker image ls
$ docker image rm [IMAGE ID]
```


## Resources Used

[gcloud CLI Reference](https://cloud.google.com/sdk/gcloud/reference/)
[Daily Smarty Blog](https://www.dailysmarty.com/posts/steps-for-deploying-a-static-html-site-with-docker-and-nginx)  
[Cloud SDK Video](https://www.youtube.com/watch?v=69MdTXgA6Ws)
